import sys
import pandas as pd
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, RDFS, XSD


def main(csv_path):
    # Read the CSV file
    print("Reading the CSV file...")
    csvdata = pd.read_csv(csv_path)
    print(f"Data from '{csv_path}' loaded successfully.")

    # Create a graph
    g = Graph()
    saref = Namespace("https://saref.etsi.org/core/")
    example = Namespace("http://example.org/")

    # Bind namespace prefixes
    g.bind("saref", saref)
    g.bind("ex", example)

    # Create RDF URI's related to the data
    building = URIRef("http://example.org/Building")
    industrial = URIRef("http://example.org/IndustrialBuilding")
    public = URIRef("http://example.org/PublicBuilding")
    residential = URIRef("http://example.org/ResidentialBuilding")
    measured_power = URIRef("http://example.org/MeasuredPower")
    metering_function = URIRef("http://example.org/MeteringFunction")

    # Define building subtypes
    g.add((industrial, RDFS.subClassOf, building))
    g.add((public, RDFS.subClassOf, building))
    g.add((residential, RDFS.subClassOf, building))
    g.add((metering_function, saref.hasMeterReadingType, measured_power))

    # Triplification
    print("Starting triplification...")
    for col_name in csvdata.columns:
        if col_name in ["utc_timestamp", "cet_cest_timestamp", "interpolated"]:
            continue

        # Building type allocation
        building_type = col_name.split('_')[2]
        building_uri = URIRef(f"http://example.org/{building_type}")

        if "industrial" in col_name:
            g.add((building_uri, RDF.type, industrial))
        elif "public" in col_name:
            g.add((building_uri, RDF.type, public))
        elif "residential" in col_name:
            g.add((building_uri, RDF.type, residential))
        else:
            print(f"Building type not recognized for column {col_name}")

        # Add meters to buildings
        meter = URIRef(f"http://example.org/{col_name}")

        g.add((meter, RDF.type, saref.Meter))
        g.add((meter, saref.measuresProperty, measured_power))
        g.add((meter, saref.hasFunction, metering_function))
        g.add((meter, RDFS.label, Literal(col_name.replace("_", " "), lang="en")))
        g.add((meter, example.locatedIn, building_uri))

        # Loop to add measurement data to meters
        print(f"Processing measurements for {col_name}...")
        for i, row in csvdata.iterrows():
            time = row["utc_timestamp"]
            value = row[col_name]
            if pd.isnull(value):
                continue

            measurement = URIRef(f"http://example.org/{col_name}_{i}")
            g.add((measurement, RDF.type, saref.Measurement))
            g.add((measurement, saref.hasValue, Literal(value, datatype=XSD.decimal)))
            g.add((measurement, saref.hasTimestamp, Literal(time, datatype=XSD.dateTime)))
            g.add((measurement, saref.isMeasuredIn, Literal("kW")))
            g.add((measurement, saref.relatesToProperty, measured_power))
            g.add((meter, saref.makesMeasurement, measurement))
            g.add((metering_function, saref.hasMeterReading, measurement))

    # Save graph as Turtle file
    print("Serializing graph to Turtle format...")
    g.serialize(destination='graph.ttl', format='turtle')
    print("Process complete! Graph saved as 'graph.ttl'")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Please provide the path to the csv as the 2nd argument")
        quit()
    else:
        csv_path = sys.argv[1]
        main(csv_path)
